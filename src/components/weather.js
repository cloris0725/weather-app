import React, {Component} from "react";
import weatherApi from "./weatherApi";
import {weatherUrl} from "../config";
import "./weather.css"
import {SearchBar} from "./searchBar";
import {WeatherTitle} from "./weatherTitle";

export default class Weather extends Component{
    state = {
        currently: {},
        daily: [],
        icon: "CLEAR_DAY",
        cityName: ""
    }

    componentDidMount() {
        this.getWeather()
    }

    getLocation(){
        if (navigator.geolocation){
            navigator.geolocation.getCurrentPosition((position) => {
                let long = position.coords.longitude.toFixed(4)
                let lat = position.coords.latitude.toFixed(4)
                this.getWeather(lat, long)
            })
        }
    }

    async getWeather(lat, long){
        let res = await weatherApi(lat,long)
        this.setState({
            currently: res.data.currently,
            daily: res.data.daily.data,
            icon: res.data.currently.icon,
            cityName: res.data.timezone

        })
        console.log(this.state.cityName)
    }


    render() {
        const {currently, daily, icon, cityName} = this.state
        return(
            <div className="container">
                <SearchBar/>
                <WeatherTitle cityname={this.state.cityName}/>
            </div>
        )
    }
}