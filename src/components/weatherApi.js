
import axios from "axios";
import {weatherUrl} from "../config"

export default async function weatherApi(lat, long){
   try{
       const res = await axios.get(`${weatherUrl}${lat}/${long}`);
       console.log(res)
       return res
   }catch (err){
       console.log(err)
   }
}


