import React from "react";


export const SearchBar = () => {
    return(
        <div className="searchBar">
            <div className="searchInput-area">
                <input type="text" name="searchCity" className="searchInput"/>

            </div>
            <div className="searchBtn-area">
                <button className="searchBtn">Search</button>
            </div>

        </div>
    )
}